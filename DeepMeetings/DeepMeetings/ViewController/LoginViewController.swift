/***************************************
 •   Name of Project : Deep Meetings
 
 •   Client Name : Imaginovation
 
 •   Name of File : LoginViewController.swift
 
 •   Date : 29/10/18.
 
 •   Original Author: Created by Vandana D.
 
 •   Contributing Authors with Feature / Update : This file is used to handle UI for Login screen. where we can put input for login with user name & password
 ***************************************/
//
//  LoginViewController.swift
//  DeepMeetings
//
//  Created by Vandana D on 29/10/18.
//  Copyright © 2018 Imaginovation. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet weak var usernameTextFiled : UITextField!
    @IBOutlet weak var passwordTextFiled : UITextField!
    @IBOutlet weak var loginScrollView : UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setup()
        self.createTapGesture(forController: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Add observers to recieve UIKeyboardWillShow and UIkeyboardWillHide notifications.
        self.addObservers()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //Remove observer to not recieve notification when viewcontroller is in background.
        self.removeObservers()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Init Methods
    
    /**
     @method : This methos is used to setup the UI for Login screen
     @parameter:
                    usernameTextFiled - Thsi field is used to input the user name
                    passwordTextFiled - This field is used to input the password
     @return: This will set up a image for the textFields
    **/
    func setup()
    {
        //set placeholder pading
        usernameTextFiled.setLeftPaddingPoints(10.0, image: #imageLiteral(resourceName: "ProfileIco"))
        passwordTextFiled.setLeftPaddingPoints(10.0, image: #imageLiteral(resourceName: "PasswordIco"))
        
    }

    //MARK: - Helper Methods
    
    func createTapGesture(forController: UIViewController) {
        let tapGesture = UITapGestureRecognizer(target: forController, action: #selector(self.dissmissKeyboard(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.cancelsTouchesInView = false
        forController.view.addGestureRecognizer(tapGesture)
    }
    
    /**
     Tap Gesture Method for dissmiss keyboard when clicks anywhere on screen
     */
    
    @objc func dissmissKeyboard(_ sender: UITapGestureRecognizer) {
        sender.view?.endEditing(true)
        //self.view.endEditing(true)
    }
    
    //MARK: - Keyboard Notification Methods
    
    /**
     Add observers for keyboard show and hide notifications
     **/
    func addObservers() {
        NotificationCenter.default.addObserver(forName: .UIKeyboardWillShow, object: nil, queue: nil) {
            (notification) in
            self.keyboardWillShow(notification: notification)
        }
        
        NotificationCenter.default.addObserver(forName: .UIKeyboardWillHide, object: nil, queue: nil) {
            (notification) in
            self.keyboardWillHide(notification: notification)
        }
    }
    
    /**
     Method remove's observers
     **/
    func removeObservers() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
        //NotificationCenter.default.removeObserver(self)
    }
    
    /**
     Method handle KeyboardWillShow notification, this method adjust's the scrollview to show hidden textfield under keyboard.
     Parameters - notification: Notification object which gets passed when keyboard is shown
     **/
      func keyboardWillShow(notification: Notification) {
        //Get the keyboard frame
        guard let userInfo = notification.userInfo,
            let frame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
                return
        }
        //Set the contentInset to scrollview to show the hidden textfield
        let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: frame.height, right: 0)
        loginScrollView.contentInset = contentInset
        loginScrollView.scrollIndicatorInsets = contentInset
    }
    
    
    /**
     Method reset's the scrollview when keyboard is hidden
     Parameters - notification: Notification object which gets passed when keyboard is about to get hidden
     **/
   func keyboardWillHide(notification: Notification) {
        let contentInset = UIEdgeInsets.zero
        loginScrollView.contentInset = contentInset
        loginScrollView.scrollIndicatorInsets = contentInset
        loginScrollView.setContentOffset(CGPoint(x: 0, y: -loginScrollView.contentInset.top), animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: - UITextField Delegate Methods
    /**
     When next or done or return key pressed this textFieldShouldReturn get called
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == passwordTextFiled
        {
             textField.resignFirstResponder()
        }
        return false; // We do not want UITextField to insert line-breaks.
        
    }

}
