//
//  AppConfiguration.swift
//  DeepMeetings
//
//  Created by Vandana D on 29/10/18.
//  Copyright © 2018 Imaginovation. All rights reserved.
//

/**
 AppConfiguration Class; Reads the values from the AppConfiguration.plist.
 Used to set the project configuration data.
 **/


import Foundation

//Keys
struct APP_CONFIGURATION_KEY {
    static let CONFIGURATION_ENVIRONMENT = "ConfigurationEnvironment"
    static let CONFIGURATION_FILE_NAME = "AppConfiguration"
    static let CONFIGURATION_FILE_TYPE = "plist"
    
    static let BASE_URL = "BaseURL"
}

class AppConfiguration {
    //MARK: - Properties
    var configuration: String!
    var variables: [String:Any]!
    
    static let sharedConfiguration = AppConfiguration()
    
    //MARK: - Initialization Methods
    
    fileprivate init() {
        //Fetch current configuration from info.plist
        let mainBundle: Bundle = Bundle.main
        configuration = mainBundle.infoDictionary?[APP_CONFIGURATION_KEY.CONFIGURATION_ENVIRONMENT] as! String
        print("Current Environment -> \(self.configuration)")
        
        //Load configuration from AppConfiguration.plist file
        let path: String = mainBundle.path(forResource: APP_CONFIGURATION_KEY.CONFIGURATION_FILE_NAME, ofType: APP_CONFIGURATION_KEY.CONFIGURATION_FILE_TYPE)!
        let configurations = NSDictionary(contentsOfFile: path) as! [String:Any]
        
        //Load data in variables based on current configuration
        variables = configurations[configuration] as! [String:Any]
        
    }
    
    //MARK: - Public static methods to access the AppConfiguration.plist keys
    
    /**
     Method returns the current environment configuration value set in info.plist
     **/
    static func currentConfiguration() -> String {
        return sharedConfiguration.configuration
    }
    
    /**
     Method returns the Base URL for the server api call.
     **/
    static func BaseURL() -> String {
        if sharedConfiguration.variables != nil {
            return sharedConfiguration.variables[APP_CONFIGURATION_KEY.BASE_URL] as! String
        }
        return ""
    }
    
}
