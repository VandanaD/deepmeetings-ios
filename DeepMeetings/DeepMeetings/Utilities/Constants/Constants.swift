//
//  Constants.swift
//  DeepMeetings
//
//  Created by Vandana D on 29/10/18.
//  Copyright © 2018 Imaginovation. All rights reserved.
//

import Foundation

struct AppStoryboad{
    static let MAIN = "main"
    static let AUTHENTICATION = "Authentication"
}


//Authentication Segue Identifier
let SEGUE_TO_LOGIN_VIEW = "segueToLoginVC"
