//
//  CommonExtension.swift
//  DeepMeetings
//
//  Created by JYOTSNA  on 29/10/18.
//  Copyright © 2018 Imaginovation. All rights reserved.
//

import UIKit


//@extension: This extension is used for TextField

extension UITextField{
    
    func setLeftPaddingPoints(_ amount:CGFloat, image: UIImage? = nil){
        if image != nil{
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.image = image!
            let paddingView = UIView(frame: CGRect(x: 20, y: 0, width: 30, height: 30))
            paddingView.addSubview(imageView)
            self.leftView = paddingView
            self.leftViewMode = .always
        }
        else
        {
            let paddingView = UIView(frame: CGRect(x: 10, y: 0, width: amount, height: self.frame.size.height))
            self.leftView = paddingView
            self.leftViewMode = .always
            self.contentVerticalAlignment = .center
        }
        
        
    }
}
